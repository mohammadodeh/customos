#!/bin/bash
# shellcheck disable=SC1091,SC2034,SC2154,SC2209,SC2317
{
	# //////////////////////////////////
	# Debian system setup using i3 (X11) wm + additional personal configurations
	# Furthermore, setup headless enabled VNC with Nvidia hardware acceleration.
	#
	# //////////////////////////////////
	# Author : Mohammad Odeh
	# Contact: mohammadodeh.com
	#
	# //////////////////////////////////

	# ==========================================================================
	# BASICS
	# ==========================================================================
	set -o errexit   					# Abort on nonzero exitstatus
	set -o nounset   					# Abort on unbound variable
	set -o pipefail  					# Don't hide errors within pipes


	# ==========================================================================
	# FLAGS, CONSTANTS, AND VARIABLES
	# ==========================================================================
	LOOP=-1
	DEBUG=true
	DRY_RUN=true
	

	# ==========================================================================
	# HELP / USAGE
	# ==========================================================================
	usage() {
		echo 'This script runs in DEBUG mode by default, only printing the commands to be executed to the screen without actually running them.'
		echo 'To install, read usage instructions below.'
		echo ' '
        echo "Usage: ./$(basename "$0") [-hn]" 2>&1
        echo '   -h   shows this help message'
        echo '   -n   dry run script'
		echo ' '
		echo '   --install   install Void Linux'
        exit 1
	}

	if [[ "${1-}" =~ ^-*h(elp)?$ ]]; then
		usage
	fi
	

	# ==========================================================================
	# COMMAND LINE PARSING
	# ==========================================================================

	# Define list of arguments expected in the input
	optstring=":hn-:"
	
	while getopts ${optstring} opt; do
		case "$opt" in
			-)
				case "${OPTARG}" in
					install)
						DEBUG=false
						DRY_RUN=false
						;;
				esac
				;;
			h)
				usage
				;;
			n)
				DRY_RUN=true
				;;
			?)
				echo "Invalid option: -${OPTARG}."
				echo
				usage
				;;
		esac
	done

	# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	# If OPTARG = n
	# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	if "$DRY_RUN"; then
		cmd=echo
	else
		cmd=${cmd:-""}
	fi
	
	# ==========================================================================
	# PREPARE ENVIRONMENT
	# ==========================================================================
	SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
	export SCRIPT_DIR
	FILE="$SCRIPT_DIR/.updated"
	if [[ -f "$FILE" ]]; then
    	echo "$FILE exists. Proceeding"
	else
		source ./func/part1/pre_run; PRE_RUN;
		touch "$FILE"
	fi

	# ==========================================================================
	# SOURCE EXTERNAL FILES
	# ==========================================================================
	source ./func/funcs-whip
	source ./func/part1/step_1
	source ./func/part1/step_2
	source ./func/part1/step_3
	source ./func/part1/step_4

	# ==========================================================================
	# TXT_HEIGHT=10;		# The height of the widget (expressed in rows)
	# TXT_WIDTH=60; 		# The width  of the widget (expressed in columns)
	# ==========================================================================
	

	# ==========================================================================
	# Get_Parent_Disk_Name()
	# USAGE:
	# List disk name and associated partitions | get second line ONLY
	# --> disk_name=$(Get_Parent_Disk_Name | sed -n '2 p')
	# ==========================================================================
	Get_Parent_Disk_Name()
	{
		lsblk | awk '/^[A-Za-z]/{d0=$1; print d0};/^[└─├─]/{d1=$1; print d0, d1};/^  [└─├─]/{d2=$1; print d0, d1, d2}' | sed 's/[├─└─]//g'

		return 0;
	}

	
	# ==========================================================================
	# Get_Parent_Disk_Name_Enhanced()
	# - .........
	# See: https://stackoverflow.com/questions/58863939/is-it-possible-to-make-a-list-of-disk-in-bash
	# ==========================================================================
	Get_Parent_Disk_Name_Enhanced()
	{
		disk=()
		size=()
		name=()
		while IFS= read -r -d $'\0' device; do
			device=${device/\/dev\//}
			disk+=("$device")
			name+=("$(cat "/sys/class/block/$device/device/model")")
			size+=("$(cat "/sys/class/block/$device/size")")
		done < <(find "/dev/" -regex '/dev/sd[a-z]\|/dev/vd[a-z]\|/dev/hd[a-z]' -print0)

		for i in $(seq 0 $((${#disk[@]}-1))); do
			echo -e "${disk[$i]}"
			# echo -e "${disk[$i]}\t${name[$i]}\t${size[$i]}"
		done
	}


	# ==========================================================================
	# Intro()
	# - .........
	# ==========================================================================
	Intro()
	{
		WHIP_BOX_TITLE="Void Linux Aarch64 Installer"

		WHIP_MENU_PROMPT="Welcome to the Void Installer for aarch64 machines"
		WHIP_MSG "$WHIP_BOX_TITLE" "$WHIP_MENU_PROMPT"

		WHIP_MENU_PROMPT="For clarity's sake, while this script does nothing complicated, unexpected things might occur.\n\nI am NOT liable for anything!"
		WHIP_MSG "$WHIP_BOX_TITLE" "$WHIP_MENU_PROMPT"

		WHIP_MENU_PROMPT="Again, I AM NOT LIABLE FOR ANYTHING!\n\nProceed?"
		if (WHIP_YESNO "$WHIP_BOX_TITLE" "$WHIP_MENU_PROMPT"); then
			return 0;
		else
			exit
		fi
	}


	# ==========================================================================
	# MAIN()
	# - .........
	# ==========================================================================
	MAIN() {
		WHIP_BOX_TITLE="Part 1/2"
		WHIP_MENU_PROMPT="Run each entry line-by-line and in order."
		WHIP_MENU_ARRAY=(
			'1' ': Partition and format disk'
			'2' ': Mount partitions'
			'3' ': Obtain ROOTFS tarball and untar'
			'4' ': Setup and enter chroot'
			'5'	': Quit'
		)

		CHOICE=$(WHIP_MENU "$WHIP_BOX_TITLE" "$WHIP_MENU_PROMPT" "${WHIP_MENU_ARRAY[@]}")

		exitstatus=$?
		if [ $exitstatus = 0 ]; then
			case $CHOICE in
				1)
					STEP_1_MAIN
					;;
				2)
					STEP_2_MAIN
					;;
				3)
					STEP_3_MAIN
					;;
				4)
					STEP_4_MAIN
					;;
				5)
					echo "Quitting"
					LOOP=0
					;;
				*)
					echo "ERROR"
					;;
			esac

		else
			$cmd echo "You chose Cancel."
			LOOP=0
		fi
	}


	# ==========================================================================
	# --- START HERE
	# ==========================================================================
	
	# Give intro
	if [[ $DEBUG == false ]]; then
		Intro
	fi

	# Get and store disk name (e.g., sdXX, vdXX, nvmeXnYpZ, etc...)
	disk_name=$(Get_Parent_Disk_Name_Enhanced)

	readonly disk_name && export disk_name

	# Start MAIN() loop
	while [ "$LOOP" != 0 ]; do
		unset -v G_PROGRAM_NAME
		MAIN
	done

	#---------------------------------------------------------------------------
	exit 0
	#---------------------------------------------------------------------------
}

#!/bin/bash
# User defined aliases are added here
#

# Easily edit i3 config files
alias i3config='nano /home/$USER/.config/i3/config'

# Go to sleep/suspend
alias peels='sudo systemctl suspend'

# Shortcuts to stuff I almost always type
alias ls='ls -la --color=auto'

#!/bin/bash

# Fixes display issue where screen is not detected
# after a while if no screen is on

# Turn off HDMI output
xrandr --output HDMI-0 --off

# Wait for a second
sleep 1

# Turn on the HDMI output
xrandr --output HDMI-0 --auto

sleep 1

xrandr --auto

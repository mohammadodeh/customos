 

pacman -Sy
pacman -Suy wget lsof dosfstools

mkfs.vfat /dev/vda1
mkswap /dev/vda2
mkfs.ext4 /dev/vda3

mount /dev/vda3 /mnt/
mkdir -p /mnt/boot/efi/
mount /dev/vda1 /mnt/boot/efi/
swapon /dev/vda2

wget https://repo-default.voidlinux.org/live/current/void-aarch64-ROOTFS-20230628.tar.xz
tar -xvf void-aarch64-ROOTFS-20230628.tar.xz -C /mnt

# -------- MANUAL CHROOT 1 --------
cp /etc/resolv.conf /mnt/etc
cp /etc/hosts /mnt/etc
mount --rbind /sys /mnt/sys && mount --make-rslave /mnt/sys
mount --rbind /dev /mnt/dev && mount --make-rslave /mnt/dev
mount --rbind /proc /mnt/proc && mount --make-rslave /mnt/proc
chroot /mnt /bin/bash
# -------- END: MANUAL CHROOT 1 --------

# -------- MANUAL CHROOT 2 --------
cp /etc/resolv.conf /mnt/etc
cp /etc/hosts /mnt/etc
mount -t proc none /mnt/proc
mount -t sysfs none /mnt/sys
mount --rbind /dev /mnt/dev
mount --rbind /run /mnt/run
chroot /mnt /bin/bash
# -------- END: MANUAL CHROOT 2 --------

#echo "nameserver 8.8.8.8">/etc/resolv.conf
xbps-install -Suy xbps
xbps-install -uy
xbps-install -uy base-system linux nano elogind polkit dbus chrony git wget curl NetworkManager
xbps-remove -y base-voidstrap


ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
xbps-reconfigure -f glibc-locales

# --------- CREATE USER ---------
echo "void" > /etc/hostname
echo "127.0.0.1    localhost
::1          localhost
127.0.1.1    void.localdomain void" >> /etc/hosts
passwd
useradd void -m -c "void" -s /bin/bash
passwd void
usermod -aG wheel,audio,video,optical,storage void
visudo # Uncomment %wheel ALL=(ALL:ALL) ALL
# --------- END: CREATE USER ---------

# Edit /etc/fstab with the bottom stuff
# /dev/vda3: /
UUID=0ca49672-4dc0-4f7a-b30a-a122d111ea25 / ext4 rw,relatime 0 1
# /dev/vda1: /boot/efi
UUID=6522-F7FC /boot/efi vfat rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=ascii,shortname=mixed,errors=remount-ro 0 2
# /dev/vda2: swap
UUID=990f5db4-349e-4246-a3b1-b3f640562a31	swap 	swap 	rw,noatime,discard 	0 0
# tmpfs
tmpfs /tmp tmpfs defaults,nosuid,nodev 0 0

xbps-install -uy grub-arm64-efi
mount -t efivarfs none /sys/firmware/efi/efivars
grub-install --target=arm64-efi --efi-directory=/boot/efi --bootloader-id="Void"
update-grub
xbps-reconfigure -fa

# ------------- DO THIS TO ENABLE AGETTY IN ORDER TO HAVE LOGIN SHELL AFTER BOOT
# If the system is not currently running, the service can be linked directly into the default runsvdir:
#
# ln -s /etc/sv/<service> /etc/runit/runsvdir/default/
#
#This will automatically start the service. Once a service is linked it will always start on boot and restart if it stops, unless administratively downed.

ln -s /etc/sv/agetty-console /etc/runit/runsvdir/default/

# Enabling RTC service
ln -s /etc/sv/chronyd /var/service/
# Enabling network-related services
ln -s /etc/sv/{dhcpcd,NetworkManager} /var/service/
# Enabling services for seat
ln -srf /etc/sv/{dbus,polkitd,elogind} /var/service

# ------ FOR NETWORKING ACCESS
sudo echo "ip link set eth0 up
dhcpcd">>/etc/rc.local
